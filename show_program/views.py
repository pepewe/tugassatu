from django.shortcuts import render
from .models import DonationProgram
from donate.models import DonationRecord

# Create your views here.


def index(request):
    context = {}
    rawDataItem = {
        'program_name': '',
        'foundation': '',
        'total': 0,
        'total_anonym_donor': 0,
        'named_donors': [],
    }
    rawNamedDonorItem = {
        'name': '',
        'value': 0,
    }
    datas = []
    donationPrograms = DonationProgram.objects.all()

    for donationProgram in donationPrograms:
        newData = rawDataItem.copy()
        newData['program_name'] = donationProgram.name
        newData['program_pictureLink'] = donationProgram.pictureLink
        newData['program_bodyNews'] = donationProgram.bodyNews
        donationRecords = DonationRecord.objects.filter(donationProgram=donationProgram.id)
        for donationRecord in donationRecords:
            newData['total'] += donationRecord.donationValue
            if donationRecord.isAnonymous is True:
                newData['total_anonym_donor'] += donationRecord.donationValue
            else:
                newNamedDonor = rawNamedDonorItem.copy()
                newNamedDonor['name'] = donationRecord.user.first_name + ' ' + donationRecord.user.last_name
                newNamedDonor['email'] = donationRecord.user.email
                newNamedDonor['value'] = donationRecord.donationValue
                newData['named_donors'].append(newNamedDonor)
        datas.append(newData)

    context['datas'] = datas
    if request.user.id is None:
        context['next_url'] = '#'
        context['user_status'] = 'not_logged_in'
        context['additional_info'] = 'type=button data-toggle=modal data-target=#exampleModal'
    else:
        context['next_url'] = '/donate/'
        context['user_status'] = 'logged_in'
    return render(request, 'show_program/home.html', context)
