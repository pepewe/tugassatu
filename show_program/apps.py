from django.apps import AppConfig


class ShowProgramConfig(AppConfig):
    name = 'show_program'
