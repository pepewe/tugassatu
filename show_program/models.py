from django.db import models

# Create your models here.


class DonationProgram(models.Model):
    name = models.CharField(max_length=200)
    pictureLink = models.CharField(max_length=200)
    bodyNews = models.TextField(max_length=20000)
