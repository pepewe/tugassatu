from django.test import TestCase
from django.contrib.auth.models import User
from django.http import HttpRequest
from .models import DonationProgram
from donate.models import DonationRecord

import show_program.views as target

# Create your tests here.


class TestProgram(TestCase):

    def setUp(self):

        self.user_ = User(
            username='meiskakurniawati99',
            first_name='Meiska',
            last_name='Kurniawati',
            email='meiskakurniawati99@gmail.com',
        )
        self.user_.set_password('apa hayo')
        self.user_.save()

        donationProgram = DonationProgram(
            name='Untukmu yang sedih',
            pictureLink='https://s3-alpha.figma.com/img/7bf4/9a1d/f79ff435609ab282c9a2fb03fa218845',
            bodyNews='mari berdonasi')
        donationProgram.save()

        donationRecord = DonationRecord.objects.create(
            user=self.user_, donationProgram=donationProgram, donationValue="100000", isAnonymous="True")
        donationRecord.save()

        donationRecord = DonationRecord.objects.create(
            user=self.user_, donationProgram=donationProgram, donationValue="100000", isAnonymous="False")
        donationRecord.save()

    def test_index(self):
        response = self.client.get('/show_program/')
        self.assertEqual(response.status_code, 200)

    def test_index_with_logged_in(self):
        request = HttpRequest()
        request.method = 'GET'
        request.user = self.user_
        response = target.index(request)
        self.assertEqual(response.status_code, 200)
