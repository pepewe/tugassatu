from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.http import HttpRequest
from .models import News
from show_program.models import DonationProgram

import donate.views

# Create your tests here.


class NewsTest(TestCase):

    def test_root_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_url_is_exist(self):
        response = Client().get('/show_news/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_news(self):
        a_user = User(
            username='rasika',
            first_name='Rasika',
            last_name='Ayuningtyas',
            email='rasika@gmail.com',
        )
        a_user.set_password('abcdef')
        a_user.save()

        donation_program = DonationProgram.objects.create(name="donasi palu")
        donation_program.save()

        # Creating a new news
        News.objects.create(
            title="Rasika",
            date="2018-10-15",
            pictureLink="https://s3-alpha.figma.com/img/4682/aeb4/1316981192c9338c1abf6cc44f740a96",
            bodyNews="ini adalah bencana",
            donationProgram=donation_program)
        # Retrieving all available activity
        count_status = News.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_database_is_not_empty(self):
        a_user = User(
            username='rasika',
            first_name='Rasika',
            last_name='Ayuningtyas',
            email='rasika@gmail.com',
        )
        a_user.set_password('abcdef')
        a_user.save()

        donation_program = DonationProgram.objects.create(name="donasi palu")
        donation_program.save()

        # Creating a new news
        new_news = News(
            title="Palu Gempa!!",
            date="2018-10-15",
            pictureLink="https://s3-alpha.figma.com/img/4682/aeb4/1316981192c9338c1abf6cc44f740a96",
            bodyNews="ini adalah bencana",
            donationProgram=donation_program)
        new_news.save()

        response = Client().get('/show_news/')
        html_response = response.content.decode('utf8')
        self.assertIn("Palu", html_response)

    def test_can_get_news(self):
        a_user = User(
            username='rasika',
            first_name='Rasika',
            last_name='Ayuningtyas',
            email='rasika@gmail.com',
        )
        a_user.set_password('abcdef')
        a_user.save()

        donation_program = DonationProgram.objects.create(name="donasi palu")
        donation_program.save()

        # Creating a new news
        new_news = News(
            title="Palu Gempa!!",
            date="2018-10-15",
            pictureLink="https://s3-alpha.figma.com/img/4682/aeb4/1316981192c9338c1abf6cc44f740a96",
            bodyNews="ini adalah bencana",
            donationProgram=donation_program)
        new_news.save()

        response = Client().get('/show_news/1/')
        html_response = response.content.decode('utf8')
        self.assertIn("Palu", html_response)


class TestDonationValue(TestCase):

    def setUp(self):
        self.user1 = User(
            username='fwp',
            first_name='Febriananda Wida',
            last_name='Pramudita',
            email='fwp@orangganteng.com',
        )
        self.user1.set_password('FwP adalah orang terganteng sedunia enggak ada yang ngalahin')
        self.user1.save()

        self.user2 = User(
            username='iisafriyanti',
            first_name='Iis',
            last_name='Afriyanti',
            email='iisafriyanti@youcanbook.me',
        )
        self.user2.set_password('PPW saya dikasih A ya Bu')
        self.user2.save()

        self.donationProgram1 = DonationProgram(
            name='Kucing Unyu',
            pictureLink='http://cdn2.tstatic.net/travel/foto/bank/images/kucing_20161122_154255.jpg',
            bodyNews='meong meong meong meong'
        )
        self.donationProgram1.save()

    def test_success_donate(self):
        request = HttpRequest()
        request.POST = {'donationProgram': self.donationProgram1.id,
                        'name': 'Febriananda Wida Pramudita',
                        'email': 'fwp@orangganteng.com',
                        'donationValue': 999123,
                        'isAnonymous': 'anonymous'}
        request.method = 'POST'
        request.user = self.user1
        donate.views.index(request)
        response = self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('Rp999.123,00', html_response)


class TestAbout(TestCase):
    def test_url_avaibility(self):
        self.assertEqual(self.client.get('/about/').status_code, 200)
