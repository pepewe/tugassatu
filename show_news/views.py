from django.shortcuts import render
from django.contrib.auth.models import User
from .models import News
# Create your views here.
from donate.views import convert_to_rupiah
from donate.models import DonationRecord
from show_program.models import DonationProgram
import datetime


def get_donation_sum(donationRecords):
    donationSum = 0
    for donationRecord in donationRecords:
        donationSum += donationRecord.donationValue
    return donationSum


def get_context_news_for_card(context, cnt):
    context['news_datas'] = []
    the_news = News.objects.all()[:cnt]
    for a_news in the_news:
        text = a_news.bodyNews.split(". ")
        spoiler_text = text[0] + "."
        context['news_datas'].append({
            'id': a_news.id,
            'title': a_news.title,
            'date': a_news.date,
            'pictureLink': a_news.pictureLink,
            'bodyNews': a_news.bodyNews,
            'spoiler': spoiler_text,
            'donationProgram': a_news.donationProgram})
    return context


def get_context_program_for_card(context, cnt):
    context['programs_datas'] = []
    the_programs = DonationProgram.objects.all()[:cnt]
    for a_program in the_programs:
        text = a_program.bodyNews.split(". ")
        spoiler_text = text[0] + "."
        context['programs_datas'].append({
            'id': a_program.id,
            'title': a_program.name,
            'date': datetime.datetime.now().date(),
            'bodyNews': a_program.bodyNews,
            'pictureLink': a_program.pictureLink,
            'spoiler': spoiler_text})
    return context


def get_news_from_news_object(request, thenews):
    context = {'attr': {
        'title': thenews.title,
        'date': thenews.date,
        'pictureLink': thenews.pictureLink,
        'bodyNews': thenews.bodyNews,
        'donationProgram': thenews.donationProgram}}
    return render(request, 'show_news/news.html', context)


def get_news(request, newsId):
    thenews = News.objects.get(pk=newsId)
    return get_news_from_news_object(request, thenews)


def home(request):
    context = {}
    get_context_news_for_card(context, 3)
    get_context_program_for_card(context, 3)
    context['money_donated'] = convert_to_rupiah(get_donation_sum(DonationRecord.objects.all()))
    context['program_funded'] = DonationProgram.objects.all().count()
    context['donator'] = User.objects.all().count()
    return render(request, 'homepage.html', context)


def index(request):
    context = {}
    get_context_news_for_card(context, News.objects.all().count())
    return render(request, 'show_news/allNews.html', context)
