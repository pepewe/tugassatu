from django.db import models
from show_program.models import DonationProgram
# Create your models here.


class News(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateField()
    pictureLink = models.CharField(max_length=200)
    bodyNews = models.TextField(max_length=20000)
    donationProgram = models.ForeignKey(DonationProgram, on_delete=models.CASCADE)
