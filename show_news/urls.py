from django.urls import path
from .views import index, get_news
# url for app
urlpatterns = [
    path('', index, name='index'),
    path('<int:newsId>/', get_news, name='get_news'),
]
