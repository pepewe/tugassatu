from django.contrib import admin
from .models import DonationRecord

# Register your models here.
admin.site.register(DonationRecord)
