from django.shortcuts import render, redirect

from .models import DonationRecord
from show_program.models import DonationProgram

# Create your views here.


def get_donation_program(donation_program_id):
    return DonationProgram.objects.get(pk=donation_program_id)


def convert_to_rupiah(num):
    rupiahNotation = ''
    while num > 0:
        rupiahNotation = '.' + str(num % 1000).rjust(3, '0') + rupiahNotation
        num //= 1000
    rupiahNotation = rupiahNotation[1:]
    while len(rupiahNotation) > 1 and rupiahNotation[0] == '0':
        rupiahNotation = rupiahNotation[1:]
    rupiahNotation = 'Rp' + rupiahNotation + ',00'
    return rupiahNotation


def get_newest_program(newestProgram):
    if DonationProgram.objects.all().count() == 0:
        newestProgram['donationProgramExist'] = False
        return

    newestProgram['donationProgramExist'] = True
    newestProgramObject = DonationProgram.objects.all().order_by('-id')[0]
    newestProgram['name'] = newestProgramObject.name
    newestProgram['pictureLink'] = newestProgramObject.pictureLink
    newestProgram['bodyNews'] = newestProgramObject.bodyNews.split('\n')
    newestProgramDonationRecords = DonationRecord.objects.filter(donationProgram=newestProgramObject)
    newestProgram['totalDonation'] = 0
    for newestProgramDonationRecord in newestProgramDonationRecords:
        newestProgram['totalDonation'] += newestProgramDonationRecord.donationValue
    newestProgram['totalDonation'] = convert_to_rupiah(newestProgram['totalDonation'])


def show_form(request, context):
    get_newest_program(context['newestProgram'])
    donationPrograms = DonationProgram.objects.all()
    for donationProgram in donationPrograms:
        context['donationPrograms'].append({'id': donationProgram.id, 'name': donationProgram.name})

    donationRecords = DonationRecord.objects.filter(isAnonymous=False).order_by('-id')[:3]
    for donationRecord in donationRecords:
        context['donationRecords'].append({
            'name': donationRecord.user.first_name + ' ' + donationRecord.user.last_name,
            'program': donationRecord.donationProgram.name,
            'value': convert_to_rupiah(donationRecord.donationValue)})
    return render(request, 'donate/donate.html', context)


def index(request):
    context = {'donationPrograms': [], 'donationRecords': [], 'newestProgram': {}, 'message': ''}
    if request.method == 'POST':
        if int(request.POST['donationValue']) <= 0:
            context['message'] = 'GAGAL MEMPROSES DONASI'
        else:
            try:
                donationProgram = get_donation_program(donation_program_id=request.POST['donationProgram'])
            except DonationProgram.DoesNotExist:
                context['message'] = 'DONASI TIDAK TERSEDIA'
                return show_form(request, context)
            newDonationRecord = DonationRecord(
                user=request.user,
                donationProgram=donationProgram,
                donationValue=request.POST['donationValue'],
                isAnonymous=(request.POST.get('isAnonymous') == 'anonymous')
            )
            newDonationRecord.save()
            context['message'] = 'BERHASIL MEMPROSES DONASI'
    if request.user.id is not None:
        return show_form(request, context)
    else:
        return redirect('/show_program/')
