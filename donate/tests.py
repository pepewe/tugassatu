from django.test import TestCase
from django.contrib.auth.models import User
from django.http import HttpRequest
from .models import DonationRecord
from show_program.models import DonationProgram

import donate.views as target


class TestUrlAvaibily(TestCase):

    def test_endpoint(self):
        response = self.client.get('/donate/')
        self.assertEqual(response.status_code, 302)

    def test_endpoint_with_login(self):
        self.user1 = User(
            username='fwp',
            first_name='Febriananda Wida',
            last_name='Pramudita',
            email='fwp@orangganteng.com',
        )
        self.user1.set_password('FwP adalah orang terganteng sedunia enggak ada yang ngalahin')
        self.user1.save()
        request = HttpRequest()
        request.method = 'GET'
        request.user = self.user1
        response = target.index(request)
        self.assertEqual(response.status_code, 200)


class TestDonate(TestCase):

    def setUp(self):
        self.user1 = User(
            username='fwp',
            first_name='Febriananda Wida',
            last_name='Pramudita',
            email='fwp@orangganteng.com',
        )
        self.user1.set_password('FwP adalah orang terganteng sedunia enggak ada yang ngalahin')
        self.user1.save()

        self.user2 = User(
            username='iisafriyanti',
            first_name='Iis',
            last_name='Afriyanti',
            email='iisafriyanti@youcanbook.me',
        )
        self.user2.set_password('PPW saya dikasih A ya Bu')
        self.user2.save()

        self.donationProgram1 = DonationProgram(
            name='Kucing Unyu',
            pictureLink='http://cdn2.tstatic.net/travel/foto/bank/images/kucing_20161122_154255.jpg',
            bodyNews='meong meong meong meong'
        )
        self.donationProgram1.save()

    def test_success_donate(self):
        initialDonationRecordCount = DonationRecord.objects.all().count()
        request = HttpRequest()
        request.POST = {'donationProgram': self.donationProgram1.id,
                        'name': 'Febriananda Wida Pramudita',
                        'email': 'fwp@orangganteng.com',
                        'donationValue': 123,
                        'isAnonymous': 'anonymous'}
        request.method = 'POST'
        request.user = self.user1
        target.index(request)
        recentDonationRecordCount = DonationRecord.objects.all().count()
        self.assertEqual(recentDonationRecordCount - initialDonationRecordCount, 1)

    def test_donation_program_not_avaiable(self):
        request = HttpRequest()
        request.POST = {'donationProgram': self.donationProgram1.id + 100,
                        'name': 'Febriananda Wida Pramudita',
                        'email': 'fwp@orangganteng.com',
                        'donationValue': 123,
                        'isAnonymous': 'anonymous'}
        request.method = 'POST'
        request.user = self.user1
        target.index(request)
        response = target.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('DONASI TIDAK TERSEDIA', html_response)

    def test_donate_not_anonymous(self):
        request = HttpRequest()
        request.POST = {'donationProgram': self.donationProgram1.id,
                        'name': 'Iis Afriyanti',
                        'email': 'iisafriyanti@youcanbook.me',
                        'donationValue': 1230, }
        request.method = 'POST'
        request.user = self.user2
        response = target.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Iis Afriyanti', html_response)

    def test_donate_with_negative_value(self):
        request = HttpRequest()
        request.POST = {'donationProgram': self.donationProgram1.id,
                        'name': 'Iis Afriyanti',
                        'email': 'iisafriyanti@youcanbook.me',
                        'donationValue': -1230}
        request.method = 'POST'
        request.user = self.user2
        response = target.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Iis Afriyanti', html_response)
