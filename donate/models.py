from django.db import models
from django.contrib.auth.models import User
from show_program.models import DonationProgram
# Create your models here.


class DonationRecord(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    donationProgram = models.ForeignKey(DonationProgram, on_delete=models.CASCADE)
    donationValue = models.IntegerField()
    isAnonymous = models.BooleanField()
