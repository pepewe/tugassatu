$(document).ready(function(){
    function loadDonation() {
        $.ajax({
                url: "/account/api/v2/load_donation/",
                success: function(result){
                    $('#totalDonation').html(`Total Donasi : ${result.sum}`);
                    text = `<br><br>`
                    for(var i = 0; i < result.records.length; i++) {
                        record = result.records[i];
                        text += `<h4>Program Donasi: ${record.donation_program}</h4><h4>Donasi: ${record.value}</h4><br>`
                    }
                    $('#account_donation_list').html(text);
                 }
            });
    }
    loadDonation();
})