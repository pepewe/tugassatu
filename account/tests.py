from django.test import TestCase
from django.contrib.auth.models import User
from django.http import HttpRequest
# Create your tests here.

from show_program.models import DonationProgram

import account.views as target
import donate.views
import json


class TestCheckAccount(TestCase):
    def setUp(self):
        self.user1 = User(
            username='fwp',
            first_name='Febriananda Wida',
            last_name='Pramudita',
            email='fwp@orangganteng.com',
        )
        self.user1.set_password('FwP adalah orang terganteng sedunia enggak ada yang ngalahin')
        self.user1.save()

        self.donationProgram1 = DonationProgram(
            name='Kucing Unyu',
            pictureLink='http://cdn2.tstatic.net/travel/foto/bank/images/kucing_20161122_154255.jpg',
            bodyNews='meong meong meong meong'
        )
        self.donationProgram1.save()

        request = HttpRequest()
        request.POST = {'donationProgram': self.donationProgram1.id,
                        'name': 'Febriananda Wida Pramudita',
                        'email': 'fwp@orangganteng.com',
                        'donationValue': 123,
                        'isAnonymous': 'anonymous'}
        request.method = 'POST'
        request.user = self.user1
        donate.views.index(request)

        request = HttpRequest()
        request.POST = {'donationProgram': self.donationProgram1.id,
                        'name': 'Febriananda Wida Pramudita',
                        'email': 'fwp@orangganteng.com',
                        'donationValue': 999000,
                        'isAnonymous': 'anonymous'}
        request.method = 'POST'
        request.user = self.user1
        donate.views.index(request)

    def test_api_account(self):
        request = HttpRequest()
        request.method = 'GET'
        request.user = self.user1
        res = json.loads(target.load_donation(request).content)
        self.assertEqual(res['sum'], 'Rp999.123,00')
        self.assertEqual(len(res['records']), 2)

    def test_url_avaibility(self):
        response = self.client.get('/account/')
        self.assertEqual(response.status_code, 200)
