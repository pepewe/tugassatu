from django.urls import re_path
from .views import index, load_donation
# url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^api/v2/load_donation/$', load_donation, name='load_donation'),
]
