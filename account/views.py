from django.http import HttpResponse
from django.shortcuts import render

from donate.models import DonationRecord
from donate.views import convert_to_rupiah

import json
# Create your views here.


def index(request):
    context = {}
    return render(request, 'account/account.html', context)


def load_donation(request):
    result = {"sum": 0, "records": []}
    preferedDonationRecords = DonationRecord.objects.filter(user=request.user)
    for donationRecord in preferedDonationRecords:
        result["records"].append({
            "donation_program": donationRecord.donationProgram.name,
            "value": donationRecord.donationValue,
        })
        result['sum'] += donationRecord.donationValue
    result['sum'] = convert_to_rupiah(result['sum'])
    return HttpResponse(json.dumps(result), content_type="application/json")
