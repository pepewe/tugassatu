# Tugas 1 PPW 2018 Kelompok 1 - Kelas E

## Anggota
- 1706028631 Febriananda Wida Pramudita

- 1706023422 Khameela Rahmah

- 1706043355 Meiska Kurniawati

- 1706039673 Rasika Ayuningtyas


## status pipeline:
[![pipeline status](https://gitlab.com/pepewe/tugassatu/badges/master/pipeline.svg)](https://gitlab.com/pepewe/tugassatu/commits/master)

## status code coverage:
[![coverage report](https://gitlab.com/pepewe/tugassatu/badges/master/coverage.svg)](https://gitlab.com/pepewe/tugassatu/commits/master)

## link heroku app: 
link percobaan => http://tugassatuppw.herokuapp.com/

link production => http://sinarperak-crowdfunding.herokuapp.com/

## Informasi Project pada Tugas 1
Pembagian tugas:
- Formulir pendaftaran untuk menjadi donatur dengan endpoint "/register/" - Khameela Rahmah
- Formulir submit donasi dengan endpoint "/donate/" - Febriananda Wida Pramudita
- Halaman yang menampilkan program dengan endpoint "/show_program/" - Meiska Kurniawati
- Halaman yang menampilkan news atau berita dengan endpoint "/show_news/" - Rasika Ayuningtyas


## Informasi Project pada Tugas 2
Pembagian tugas:
- Login  with Google -- Rasika Ayuningtyas
- Daftar List Program (tugas 1) - tombol donasi kalau sudah login diklik langsung donate, kalau belum login diklik minta login -- Febriananda Wida Pramudita
- Donasi ke suatu program (bagi yang sudah login) -- Febriananda Wida Pramudita
- Daftar Donasi yang pernah dilakukan (bagi yang sudah login) -- Meiska Kurniawati
- Halaman About -- Khameela Rahmah
- Testimoni untuk halaman about -- Khameela Rahmah
