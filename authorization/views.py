from django.shortcuts import redirect, render
from django.contrib.auth import logout


def logout_user(request):
    logout(request)
    return redirect(request.GET.get('next', '/'))


def complete(request):
    context = {}
    return render(request, 'authorization/complete.html', context)
