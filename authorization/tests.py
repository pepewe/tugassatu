from django.test import TestCase
from django.contrib.auth.models import User
from django.http import HttpRequest

import authorization.views as target


class TestApp(TestCase):

    def test_logout(self):
        self.user1 = User(
            username='fwp',
            first_name='Febriananda Wida',
            last_name='Pramudita',
            email='fwp@orangganteng.com',
        )
        self.user1.set_password('FwP adalah orang terganteng sedunia enggak ada yang ngalahin')
        self.user1.save()
        request = HttpRequest()
        request.method = 'GET'
        request.session = self.client.session
        request.user = self.user1
        response = target.logout_user(request)
        self.assertEqual(response.status_code, 302)

    def test_complete_gateway_is_exist(self):
        response = self.client.get('/authorization/complete/')
        self.assertEqual(response.status_code, 200)
