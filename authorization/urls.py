from django.urls import re_path
from .views import logout_user, complete
# url for app
urlpatterns = [
    re_path(r'^logout_user/$', logout_user, name='logout_user'),
    re_path(r'^complete/$', complete, name='complete'),
]
