"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
import show_news.views

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^$', show_news.views.home),
    re_path(r'^about/', include('about.urls')),
    re_path(r'^show_news/', include('show_news.urls')),
    re_path(r'^authorization/', include('authorization.urls')),
    re_path(r'^donate/', include('donate.urls')),
    re_path(r'^show_program/', include('show_program.urls')),
    re_path(r'^register/', include('register.urls')),
    re_path(r'^account/', include('account.urls')),
    re_path(r'^auth/', include('social_django.urls', namespace='social')),
]
