$(document).ready(function(){
    function loadTestimony() {
        $.ajax({
                url: "/about/api/v2/load_testimony/",
                success: function(result){
                    text = ``
                    for(var i = 0; i < result.length; i++) {
                        text += `<h5>${result[i].name} berkata</h5>${result[i].testimony}<br><br>`
                    }
                    $('#testimony_list').html(text);
                 }
            });
    }
    loadTestimony();
    $("#addTestimony").submit(function(e) {
        var frm = $(this)
        e.preventDefault();
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (result) {
                alert(result.message);
                loadTestimony();
                $('#testimonyText').val('')
            },
        });
    });
})