from django.urls import re_path
from .views import register
# url for app
urlpatterns = [
    re_path(r'^$', register, name='register'),
]
