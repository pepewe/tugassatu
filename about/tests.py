from django.test import TestCase
from django.contrib.auth.models import User
from django.http import HttpRequest
from about.models import Testimony

import about.views as target
import json


class TestUrlAvaibily(TestCase):

    def test_endpoint(self):
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)


class TestApiTestimony(TestCase):
    def setUp(self):
        self.user1 = User(
            username='fwp',
            first_name='Febriananda Wida',
            last_name='Pramudita',
            email='fwp@orangganteng.com',
        )
        self.user1.set_password('FwP adalah orang terganteng sedunia enggak ada yang ngalahin')
        self.user1.save()

        self.user2 = User(
            username='iisafriyanti',
            first_name='Iis',
            last_name='Afriyanti',
            email='iisafriyanti@youcanbook.me',
        )
        self.user2.set_password('PPW saya dikasih A ya Bu')
        self.user2.save()

        self.testimony1 = Testimony(user=self.user2, testimony='Mantap.\nDapet A nih PPW-nya.')
        self.testimony1.save()

    def test_load_api(self):
        request = HttpRequest()
        request.method = 'GET'
        request.user = self.user1
        result = json.loads(target.load_testimony(request).content)
        self.assertEqual(len(result), 1)

    def test_add_api_success(self):
        request = HttpRequest()
        request.POST = {'testimonyText': 'Bismillah PPW kelar. Semoga saja. Tidaaaaaaak, meonggggg'}
        request.method = 'POST'
        request.user = self.user1
        result = json.loads(target.add_testimony(request).content)
        self.assertEqual(result['message'], 'SUCCESS')

    def test_add_api_fail(self):
        request = HttpRequest()
        request.GET = {'testimonyText': 'Bismillah PPW kelar. Semoga saja. Tidaaaaaaak, meonggggg'}
        request.method = 'GET'
        request.user = self.user1
        result = json.loads(target.add_testimony(request).content)
        self.assertEqual(result['message'], 'FAIL')
