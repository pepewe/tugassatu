from django.shortcuts import render
from django.http import HttpResponse
from .models import Testimony
import json
# Create your views here.


def index(request):
    context = {}
    return render(request, 'about/about.html', context)


def html_encode(raw_text):
    # to prevent xss
    res = ''
    for c in raw_text:
        if c == '\n':
            res += '<br>'
        else:
            res += '&#{};'.format(ord(c))
    return res


def load_testimony(request):
    testimony_list = []
    testimonies = Testimony.objects.all()
    for testimony in testimonies:
        testimony_list.append({'name': testimony.user.first_name + ' ' + testimony.user.last_name,
                               'testimony': html_encode(testimony.testimony)})
    return HttpResponse(json.dumps(testimony_list), content_type='application/json')


def add_testimony(request):
    status = {}
    if request.method == 'POST':
        newTestimony = Testimony(user=request.user, testimony=request.POST.get('testimonyText'))
        newTestimony.save()
        status['message'] = 'SUCCESS'
        return HttpResponse(json.dumps(status), content_type='application/json')
    status['message'] = 'FAIL'
    return HttpResponse(json.dumps(status), content_type='application/json')
