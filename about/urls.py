from django.urls import re_path
from .views import index, load_testimony, add_testimony
# url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^api/v2/load_testimony/$', load_testimony, name='load_testimony'),
    re_path(r'^api/v2/add_testimony/$', add_testimony, name='add_testimony'),
]
