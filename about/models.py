from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Testimony(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    testimony = models.TextField(max_length=2000)
